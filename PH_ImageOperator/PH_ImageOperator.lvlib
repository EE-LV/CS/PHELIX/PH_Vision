﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Description" Type="Str">This class is forseen to operate on an image: it calculates several beam profile values. This class is connected to the class PH_ImageCalibrator which has to be used to produce a background image.

This class is part of the PHELIX user layer.

author: Stefan Götte, GSI

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation; either version 2 of 
the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 17-JUN-2008</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!*Q(C=\&gt;1^&lt;BJ2&amp;-8RYSB&amp;7ALX%3OQ&gt;&amp;=1C1V15'1$&gt;QOU,GF4HC79BA7QB&gt;OG3-%7W),THT=8#^F3;')JCDS0"].Z8T^GBC?.YYPU7=_X$PXRI(UKYT7[^MEU=0[=WM?XO@^,[@/L^KPMOLZNHZ?Z'P^K@@8[0&gt;=.`U@\2`N`X(\\\`VYM^-\(^,&gt;:==IKKF*D8E(+0)C,`)C,`)C.\H*47ZSEZM]S:-]S:-]S:-]S)-]S)-]S//SIZ#,8/3+M9-5CR=,&amp;:-7%R3&gt;I;BY+TS&amp;J`!5(L[K]"3?QF.Y#A^&gt;6(A+4_%J0)7(93I]B;@Q&amp;*\#QV2$5K/1YSE]4#`D-2\D-2\D95E:DQ'9R=T%:B)9-IXGR(C-RXAYF@%9D`%9D`(1,/-R(O-R(O.BS,AK(JKJE/.B'C7?R*.Y%E`C97IFHM34?"*0YG%Z*:\%ES#3":0*)3A:F(2)PC3?R-/(%E`C34S**`(1./Z1DCMT;;:#DC@Q"*\!%XA#$V-I]!3?Q".Y!A`4+P!%HM!4?!)03SHQ"*\!%U##26F?Q74"Q+"4%!1?8O.JC8'80#1R3P]VZQ&gt;6`1#K(STV![._%.1X7(XDV$&gt;%@;(6&amp;V"^9&gt;1`70V$V%$VQOI*V2VVZPV%06,XV"VV3^V16^1F&gt;4%.`=M&gt;T_?T4K?4DM?D^PO^&gt;LO&gt;NNON.JO.6KO6FMOF&amp;IP&amp;SW\VF4)@&gt;Z&gt;^[:(TJQ@`/HT&lt;X"`7XXV9L__@(H\]H0,XL0/_^#`MD@KEZ[NNHGPU'^%O0_A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Analyse" Type="Folder">
			<Item Name="Analysis" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="Convex Hull Analysis" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="PH_ImageOperator.CH Analysis Calc Fillfactor.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis Calc Fillfactor.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis check Settings.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis check Settings.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis ConvexHull.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis ConvexHull.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis draw Analysis.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis draw Analysis.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis Fillfactor Dataset.ctl" Type="VI" URL="../PH_ImageOperator.CH Analysis Fillfactor Dataset.ctl"/>
					<Item Name="PH_ImageOperator.CH Analysis Fit Ellipse.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis Fit Ellipse.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis MaxEnergy.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis MaxEnergy.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis pack Data.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis pack Data.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis set GUI Elements visible.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis set GUI Elements visible.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis set Settings.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis set Settings.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis Settings.ctl" Type="VI" URL="../PH_ImageOperator.CH Analysis Settings.ctl"/>
					<Item Name="PH_ImageOperator.CH Analysis ThresholdImage.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis ThresholdImage.vi"/>
					<Item Name="PH_ImageOperator.CH Analysis.vi" Type="VI" URL="../PH_ImageOperator.CH Analysis.vi"/>
				</Item>
				<Item Name="DIN Analysis" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="Trace Analysis" Type="Folder">
						<Item Name="PH_ImageOperator.TA Analysis.vi" Type="VI" URL="../PH_ImageOperator.TA Analysis.vi"/>
						<Item Name="PH_ImageOperator.TA Best GAUSS Procedure.vi" Type="VI" URL="../PH_ImageOperator.TA Best GAUSS Procedure.vi"/>
						<Item Name="PH_ImageOperator.TA calc width diameter.vi" Type="VI" URL="../PH_ImageOperator.TA calc width diameter.vi"/>
						<Item Name="PH_ImageOperator.TA check Settings.vi" Type="VI" URL="../PH_ImageOperator.TA check Settings.vi"/>
						<Item Name="PH_ImageOperator.TA CreateTrace.vi" Type="VI" URL="../PH_ImageOperator.TA CreateTrace.vi"/>
						<Item Name="PH_ImageOperator.TA Dataset.ctl" Type="VI" URL="../PH_ImageOperator.TA Dataset.ctl"/>
						<Item Name="PH_ImageOperator.TA Fit Gauss.vi" Type="VI" URL="../PH_ImageOperator.TA Fit Gauss.vi"/>
						<Item Name="PH_ImageOperator.TA GaussFit.vi" Type="VI" URL="../PH_ImageOperator.TA GaussFit.vi"/>
						<Item Name="PH_ImageOperator.TA get Traces.vi" Type="VI" URL="../PH_ImageOperator.TA get Traces.vi"/>
						<Item Name="PH_ImageOperator.TA Lev-Mar abx.vi" Type="VI" URL="../PH_ImageOperator.TA Lev-Mar abx.vi"/>
						<Item Name="PH_ImageOperator.TA Lev-Mar xx.vi" Type="VI" URL="../PH_ImageOperator.TA Lev-Mar xx.vi"/>
						<Item Name="PH_ImageOperator.TA pack Data.vi" Type="VI" URL="../PH_ImageOperator.TA pack Data.vi"/>
						<Item Name="PH_ImageOperator.TA Reduce Fit Trace.vi" Type="VI" URL="../PH_ImageOperator.TA Reduce Fit Trace.vi"/>
						<Item Name="PH_ImageOperator.TA set GUI Elements visible.vi" Type="VI" URL="../PH_ImageOperator.TA set GUI Elements visible.vi"/>
						<Item Name="PH_ImageOperator.TA set Settings.vi" Type="VI" URL="../PH_ImageOperator.TA set Settings.vi"/>
						<Item Name="PH_ImageOperator.TA Settings.ctl" Type="VI" URL="../PH_ImageOperator.TA Settings.ctl"/>
						<Item Name="PH_ImageOperator.TA Target Fnc and Deriv GAUSSs.vi" Type="VI" URL="../PH_ImageOperator.TA Target Fnc and Deriv GAUSSs.vi"/>
					</Item>
					<Item Name="PH_ImageOperator.DIN Analysis Border Control.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis Border Control.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis check Settings.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis check Settings.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis draw Analysis.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis draw Analysis.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis get Azimuth Angle.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis get Azimuth Angle.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis get Beam Dimensions.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis get Beam Dimensions.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis get COG.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis get COG.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis get Covariance.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis get Covariance.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis get First Guess ROI.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis get First Guess ROI.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis get Variance.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis get Variance.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis pack Data.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis pack Data.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis Routine.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis Routine.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis set GUI Elements visible.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis set GUI Elements visible.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis set Settings.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis set Settings.vi"/>
					<Item Name="PH_ImageOperator.DIN Analysis Settings.ctl" Type="VI" URL="../PH_ImageOperator.DIN Analysis Settings.ctl"/>
					<Item Name="PH_ImageOperator.DIN Analysis.vi" Type="VI" URL="../PH_ImageOperator.DIN Analysis.vi"/>
				</Item>
				<Item Name="Position Analysis" Type="Folder">
					<Item Name="PH_ImageOperator.PA Analysis.vi" Type="VI" URL="../PH_ImageOperator.PA Analysis.vi"/>
					<Item Name="PH_ImageOperator.PA check Settings.vi" Type="VI" URL="../PH_ImageOperator.PA check Settings.vi"/>
					<Item Name="PH_ImageOperator.PA Dataset.ctl" Type="VI" URL="../PH_ImageOperator.PA Dataset.ctl"/>
					<Item Name="PH_ImageOperator.PA get Analysis.vi" Type="VI" URL="../PH_ImageOperator.PA get Analysis.vi"/>
					<Item Name="PH_ImageOperator.PA get Offset.vi" Type="VI" URL="../PH_ImageOperator.PA get Offset.vi"/>
					<Item Name="PH_ImageOperator.PA get Value.vi" Type="VI" URL="../PH_ImageOperator.PA get Value.vi"/>
					<Item Name="PH_ImageOperator.PA pack Data.vi" Type="VI" URL="../PH_ImageOperator.PA pack Data.vi"/>
					<Item Name="PH_ImageOperator.PA set GUI Elements visible.vi" Type="VI" URL="../PH_ImageOperator.PA set GUI Elements visible.vi"/>
					<Item Name="PH_ImageOperator.PA set Settings.vi" Type="VI" URL="../PH_ImageOperator.PA set Settings.vi"/>
					<Item Name="PH_ImageOperator.PA set Unit.vi" Type="VI" URL="../PH_ImageOperator.PA set Unit.vi"/>
					<Item Name="PH_ImageOperator.PA Settings.ctl" Type="VI" URL="../PH_ImageOperator.PA Settings.ctl"/>
				</Item>
				<Item Name="PH_ImageOperator.Analysis check Calibration.vi" Type="VI" URL="../PH_ImageOperator.Analysis check Calibration.vi"/>
				<Item Name="PH_ImageOperator.Analysis Dataset.ctl" Type="VI" URL="../PH_ImageOperator.Analysis Dataset.ctl"/>
				<Item Name="PH_ImageOperator.Analysis draw Analysis.vi" Type="VI" URL="../PH_ImageOperator.Analysis draw Analysis.vi"/>
				<Item Name="PH_ImageOperator.Analysis get major and minor Axis.vi" Type="VI" URL="../PH_ImageOperator.Analysis get major and minor Axis.vi"/>
				<Item Name="PH_ImageOperator.Analysis get Value.vi" Type="VI" URL="../PH_ImageOperator.Analysis get Value.vi"/>
				<Item Name="PH_ImageOperator.Analysis set Unit.vi" Type="VI" URL="../PH_ImageOperator.Analysis set Unit.vi"/>
			</Item>
			<Item Name="Energy Analysis" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="PH_ImageOperator.EA Analysis.vi" Type="VI" URL="../PH_ImageOperator.EA Analysis.vi"/>
				<Item Name="PH_ImageOperator.EA check Settings.vi" Type="VI" URL="../PH_ImageOperator.EA check Settings.vi"/>
				<Item Name="PH_ImageOperator.EA Dataset.ctl" Type="VI" URL="../PH_ImageOperator.EA Dataset.ctl"/>
				<Item Name="PH_ImageOperator.EA get Energy Vaue.vi" Type="VI" URL="../PH_ImageOperator.EA get Energy Vaue.vi"/>
				<Item Name="PH_ImageOperator.EA pack Data.vi" Type="VI" URL="../PH_ImageOperator.EA pack Data.vi"/>
				<Item Name="PH_ImageOperator.EA set Energy Unit.vi" Type="VI" URL="../PH_ImageOperator.EA set Energy Unit.vi"/>
				<Item Name="PH_ImageOperator.EA set GUI Elements visible.vi" Type="VI" URL="../PH_ImageOperator.EA set GUI Elements visible.vi"/>
				<Item Name="PH_ImageOperator.EA set Settings.vi" Type="VI" URL="../PH_ImageOperator.EA set Settings.vi"/>
				<Item Name="PH_ImageOperator.EA Settings.ctl" Type="VI" URL="../PH_ImageOperator.EA Settings.ctl"/>
			</Item>
			<Item Name="PH_ImageOperator.Analyse.vi" Type="VI" URL="../PH_ImageOperator.Analyse.vi"/>
			<Item Name="PH_ImageOperator.Draw Image.vi" Type="VI" URL="../PH_ImageOperator.Draw Image.vi"/>
			<Item Name="PH_ImageOperator.Subtract Background Image.vi" Type="VI" URL="../PH_ImageOperator.Subtract Background Image.vi"/>
		</Item>
		<Item Name="PSDB" Type="Folder">
			<Item Name="PH_ImageOperator.write PSDB Analysis data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB Analysis data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB Calibration data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB Calibration data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB Cam Settings data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB Cam Settings data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB Convex Hull Analysis data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB Convex Hull Analysis data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB DIN Analysis data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB DIN Analysis data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB Energy Analysis data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB Energy Analysis data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB Images data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB Images data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB Position data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB Position data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB Trace Analysis data.vi" Type="VI" URL="../PH_ImageOperator.write PSDB Trace Analysis data.vi"/>
			<Item Name="PH_ImageOperator.write PSDB.vi" Type="VI" URL="../PH_ImageOperator.write PSDB.vi"/>
		</Item>
		<Item Name="PH_ImageOperator.allocate or free img space.vi" Type="VI" URL="../PH_ImageOperator.allocate or free img space.vi"/>
		<Item Name="PH_ImageOperator.check Cam Settings.vi" Type="VI" URL="../PH_ImageOperator.check Cam Settings.vi"/>
		<Item Name="PH_ImageOperator.get Cam Settings.vi" Type="VI" URL="../PH_ImageOperator.get Cam Settings.vi"/>
		<Item Name="PH_ImageOperator.i attribute.ctl" Type="VI" URL="../PH_ImageOperator.i attribute.ctl"/>
		<Item Name="PH_ImageOperator.i attribute.vi" Type="VI" URL="../PH_ImageOperator.i attribute.vi"/>
		<Item Name="PH_ImageOperator.PC Read Cali Data.vi" Type="VI" URL="../PH_ImageOperator.PC Read Cali Data.vi"/>
		<Item Name="PH_ImageOperator.PC set Global On.vi" Type="VI" URL="../PH_ImageOperator.PC set Global On.vi"/>
		<Item Name="PH_ImageOperator.ProcEvents.vi" Type="VI" URL="../PH_ImageOperator.ProcEvents.vi"/>
		<Item Name="PH_ImageOperator.publish Calibration Data.vi" Type="VI" URL="../PH_ImageOperator.publish Calibration Data.vi"/>
		<Item Name="PH_ImageOperator.publish Data.vi" Type="VI" URL="../PH_ImageOperator.publish Data.vi"/>
		<Item Name="PH_ImageOperator.set GUI elements for the first time.vi" Type="VI" URL="../PH_ImageOperator.set GUI elements for the first time.vi"/>
		<Item Name="PH_ImageOperator.set i Attr and start DIM Services.vi" Type="VI" URL="../PH_ImageOperator.set i Attr and start DIM Services.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_ImageOperator.get i attribute.vi" Type="VI" URL="../PH_ImageOperator.get i attribute.vi"/>
		<Item Name="PH_ImageOperator.ProcCases.vi" Type="VI" URL="../PH_ImageOperator.ProcCases.vi"/>
		<Item Name="PH_ImageOperator.ProcPeriodic.vi" Type="VI" URL="../PH_ImageOperator.ProcPeriodic.vi"/>
		<Item Name="PH_ImageOperator.set i attribute.vi" Type="VI" URL="../PH_ImageOperator.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_ImageOperator.constructor.vi" Type="VI" URL="../PH_ImageOperator.constructor.vi"/>
		<Item Name="PH_ImageOperator.destructor.vi" Type="VI" URL="../PH_ImageOperator.destructor.vi"/>
		<Item Name="PH_ImageOperator.DIM Service Names.vi" Type="VI" URL="../PH_ImageOperator.DIM Service Names.vi"/>
		<Item Name="PH_ImageOperator.evt call Read Cali Data.vi" Type="VI" URL="../PH_ImageOperator.evt call Read Cali Data.vi"/>
		<Item Name="PH_ImageOperator.evt call set Convex Hull Analysis Settings.vi" Type="VI" URL="../PH_ImageOperator.evt call set Convex Hull Analysis Settings.vi"/>
		<Item Name="PH_ImageOperator.evt call set DIN Settings.vi" Type="VI" URL="../PH_ImageOperator.evt call set DIN Settings.vi"/>
		<Item Name="PH_ImageOperator.evt call set Energy Settings.vi" Type="VI" URL="../PH_ImageOperator.evt call set Energy Settings.vi"/>
		<Item Name="PH_ImageOperator.evt call set Global On.vi" Type="VI" URL="../PH_ImageOperator.evt call set Global On.vi"/>
		<Item Name="PH_ImageOperator.evt call set Position Settings.vi" Type="VI" URL="../PH_ImageOperator.evt call set Position Settings.vi"/>
		<Item Name="PH_ImageOperator.evt call set Trace Analysis Settings.vi" Type="VI" URL="../PH_ImageOperator.evt call set Trace Analysis Settings.vi"/>
		<Item Name="PH_ImageOperator.get data to modify.vi" Type="VI" URL="../PH_ImageOperator.get data to modify.vi"/>
		<Item Name="PH_ImageOperator.panel.vi" Type="VI" URL="../PH_ImageOperator.panel.vi"/>
		<Item Name="PH_ImageOperator.set modified data.vi" Type="VI" URL="../PH_ImageOperator.set modified data.vi"/>
	</Item>
	<Item Name="PH_ImageOperator.contents.vi" Type="VI" URL="../PH_ImageOperator.contents.vi"/>
</Library>
